-module(ueb_161124).
-compile(export_all).


%eine fun die auf Basis von filter und length berechnen ob eine Zahl eine Primzahl ist

%Idee: eine Liste von 1 bis sqrt(X) : Liste = lists:seq(1, math:sqrt(X)).
%fun(Z)->X REM Z == 0 end.

%wenn die liste nach dem remainder test mehr als ein element hat ist es keine primzahl

%Prim(X)->filter()




%2. eine Listen Faltung
% aggregation über eine Liste
% liste = (1,2,3,4,5,6)
% basis bei addition 0 so dass -> 0+1=1; dann erg+2=3, erg+3= usw
% basis bei multiplikation 1 so dass -> 1*1=1, erg*2=2, erg*3 = 6 usw

AggregationsListe = (1,2,3,4,5,6).

% sowas wie fold(fun(X,Y)->X+Y), Base, List).


fold(_,Base,[])->Base
fold(foldFun, Base, [X|XS])-> foldFun(X, fold(foldFun, Base, XS)).


%2b -> Reduce bauen. Das gleiche ohne Basis. Den Head als Basis verwenden