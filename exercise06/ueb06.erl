-module(ueb06).
-export([getProg/2,getSingleParadigm/2, getParadigm/2, addToList/2]).



%Exercise 01:

%Prerequisites:
%Paradigmen = [{erlang, funktional}, {erlang, logisch}, {prolog, logisch}, {scala, functional}, {scala, objectorientiert}, {scala, logisch}, {java, objectorientiert}].

%a) @params Typ, type of language, Paradigmen, list of paradigms
getProg(Typ, [H|T])->getProg(Typ, [H|T], []).
getProg(_, [], L)-> L;
getProg(Typ, [{Sprache,Typ}|T], L)->getProg(Typ, T, [Sprache|L]);
getProg(Typ, [_|T], L)->getProg(Typ, T, L).

%b) first, find by "one" language
getSingleParadigm(Sprache, Paradigmen)->getSingleParadigm(Sprache,Paradigmen,[]).
%Abbruch
getSingleParadigm(_,[],L)->L;
%Gefunden
getSingleParadigm(Sprache,[{Sprache,Typ}|T], L)->getSingleParadigm(Sprache,T,[Typ|L]);
%Nicht Gefunden
getSingleParadigm(Sprache,[_|T],L)->getSingleParadigm(Sprache,T,L).


%b2) get many languages
getParadigm(Sprachen, Paradigmen)->getParadigm(Sprachen,Paradigmen,[]).
getParadigm([], _, L)->L;	%Abbruch
getParadigm([H|T], Paradigmen, L)->getParadigm(T, Paradigmen, [getSingleParadigm(H, Paradigmen)|L]).



%c) get a List of Tuples which hold as one value the language and list holding all paradigmns

%utility:

%add language to output list
% Liste leer ? adde.
% Liste hat Inhalt ? Sprache finden und feature adden.
% Sprache nicht drin ? Sprache plus Feature anhängen.

%entry point
addToList({Sprache, Feature}, Outputlist)->addToList({Sprache, Feature}, Outputlist, []).
%Liste noch leer ?
addToList({Sprache, Feature}, [], [])->[{Sprache,[Feature]}];
%Liste nicht leer ? finde Sprache
%Sprache gefunden ? adde feature
addToList({Sprache, Feature}, [{Sprache, Features}|T], ReturnList)->[[{Sprache,[Feature|Features]}|T]|ReturnList];
%Sprache nicht gefunden ? Liste durchgehen
addToList({Sprache, Feature}, [H|T], ReturnList)->addToList({Sprache, Feature}, T, [H|ReturnList]);
%Sprache nicht in Liste ? Sprache & Feature anlegen
addToList({Sprache, Feature}, [], ReturnList)->[[{Sprache,[Feature]}]|ReturnList].




