-module(ueb03).
-export([fibo/1, fiboTail/1, member/2, intoList/2, intoList2/2, quersumme/1, listSort/2]).
%-compile(export_all).


%Excercise #1:

%fn = fn-1 + fn-2 für n>2
%0,1,1,2,3,5,8,13...

%cheap and not correct but better way follows beyond
fibo(0)->0;
fibo(1)->1;
fibo(X)->fibo(X-1)+fibo(X-2).


%Excercise #2:

%tail rekursiv
fiboTail(X)->fiboTail(0, 1 ,X).
fiboTail(X,Y,2)->X+Y;
fiboTail(X,Y,Z)->fiboTail(Y,X+Y,Z-1).


%Excercise #4:

%soll element wenn vorhanden zurück geben
%X = elem, L = Liste

%member(X,[Head|Tail])when X == Head -> true;
member(X,[X|Tail]) -> true;
member(X,[])-> false;
%member(X,[Head|Tail])when X /= Head -> member(X,Tail).
member(X,[_|Tail]) -> member(X,Tail).


%Excercise #5

%insert Var into sorted List
intoList(X, [H|T])->intoList(X, [H|T], []).
intoList(X, [], NeueListe)->NeueListe;
intoList(X, [H|T], NeueListe)when H < X->intoList(X, T, [H|NeueListe]);
intoList(X, [H|T], NeueListe)->intoList(inserted, [H|T],[X|NeueListe]);
intoList(inserted, AlteListe, NeueListe)->[NeueListe|AlteListe].


%insert var into sorted list shorter way
intoList2(X, [H|T])when X =< H ->[X|[H|T]];
intoList2(X, [])->[X];
intoList2(X, [H|T])when X > H ->[H|intoList2(X,T)].


%Excercise #6:

%berechnen der Quersumme
quersumme(X) -> quersumme(X,0).
quersumme(X,Y) when X < 10 -> X + Y;
quersumme(X,Y) when X >= 10 -> quersumme(trunc(X/10),Y + (X rem 10)).


%Exc #7
listSort(X, [H|T])->listSort(X, [H|T], []).
listSort(X, [], NeueListe)->intoList2(X, NeueListe);
listSort(X, [H|T], NeueListe)->listSort(X, T, intoList2(H, NeueListe)).
